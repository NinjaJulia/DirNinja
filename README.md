# DirNinja

A web file and directory bruteforce tester for analyzing webpages built in Java.

This terminal application loves to run in Linux because it will output in color to *nix terminal. This will work in Windows without colors. 

## Help errata:

DirNinja help:

$ java DirNinja -u url (optional) -w wordlist -x extension(s) -o MySiteDirs.txt

[ -h or --help ] display this errata and exit

[ -u or --url ] the url to scan

[ -w or --wordlist ] the wordlist to use for scanning
    if unused will default to using dirb's common dictionary

[ -x or --extensions ] extension(s) to use while scanning
    note that extensions can be a string or a file containing 
    extensions you want to test.Iif you want to provide more 
	than one, you may use -x or --extensions more than once.

[ -b or --bruteforce ] scans the url in bruteforce mode.
    optional bruteforce setting -d or --depth will set the max bruteforce depth (Max 12)
    optional bruteforce setting -a or --alphabet will select the bruteforce alphabet
    from the following: all, alphanum, num, alpha, spec, dash
    default settings are a maximum depth of 6 characters and alphabet alphanum

[ -o or --out ] saves the scan results to a file
    provide a filename such as MySiteDirs.txt to save the output

[ -s or --single ] does not follow directories it finds
    scans this directory only

### examples:

###### java DirNinja -u http://somesite.com

###### java DirNinja -u http://somesite.com -w /usr/share/wordlists/dirb/small -x php

###### java DirNinja -u https://somesite.com -a username:password -b -d 6 -a alphanum

###### java DirNinja -u http://somesite.com/secrets/ -s -b -d 6 -a all -x extensions.txt

